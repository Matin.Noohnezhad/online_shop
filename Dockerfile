FROM maven:3.8.5-openjdk-18

WORKDIR /tmp/online_shop
ENTRYPOINT ["mvn", "spring-boot:run"]
