package com.matin.online_shop.config;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQConfig {

    @Value("${onlineshop.rabbitmq.user_creation_queue}")
    String USER_CREATION_QUEUE;

    @Value("${onlineshop.rabbitmq.exchange}")
    String USER_CREATION_EXCHANGE;

    @Value("${onlineshop.rabbitmq.routingkey}")
    private String ROUTING_KEY;
    //
    @Value("${onlineshop.rabbitmq.user_payment_rpc_queue1}")
    String RPC_QUEUE1;

    @Value("${onlineshop.rabbitmq.user_payment_rpc_queue2}")
    String RPC_QUEUE2;

    @Value("${onlineshop.rabbitmq.rpc_exchange}")
    String RPC_EXCHANGE;

    @Value("${onlineshop.rabbitmq.rpc_routingkey1}")
    String RPC_ROUTING_KEY1;

    @Value("${onlineshop.rabbitmq.rpc_routingkey2}")
    String RPC_ROUTING_KEY2;

    @Bean
    Queue queue() {
        return new Queue(USER_CREATION_QUEUE, false);
    }

    @Bean
    DirectExchange exchange() {
        return new DirectExchange(USER_CREATION_EXCHANGE);
    }

    @Bean
    Binding binding(Queue queue, DirectExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(ROUTING_KEY);
    }

    @Bean
    public MessageConverter jsonMessageConverter() {
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    @Qualifier("user_creation_template")
    public AmqpTemplate rabbitTemplate(ConnectionFactory connectionFactory) {
        final RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(jsonMessageConverter());
        return rabbitTemplate;
    }

    @Bean
    Queue rpcMsgQueue() {
        return new Queue(RPC_QUEUE1);
    }

    @Bean
    Queue rpcReplyQueue() {
        return new Queue(RPC_QUEUE2);
    }

    @Bean
    DirectExchange rpcExchange() {
        return new DirectExchange(RPC_EXCHANGE);
    }

    @Bean
    Binding rpcMsgBinding() {
        return BindingBuilder.bind(rpcMsgQueue()).to(rpcExchange()).with(RPC_ROUTING_KEY1);
    }

    @Bean
    Binding rpcReplyBinding() {
        return BindingBuilder.bind(rpcReplyQueue()).to(rpcExchange()).with(RPC_ROUTING_KEY2);
    }

    @Bean
    @Qualifier("rpc_template")
    RabbitTemplate rpcRabbitTemplate(ConnectionFactory connectionFactory) {
        RabbitTemplate template = new RabbitTemplate(connectionFactory);
        template.setReplyAddress(RPC_QUEUE2);
        template.setReplyTimeout(100000);
        template.setMessageConverter(jsonMessageConverter());
        return template;
    }

    @Bean
    SimpleMessageListenerContainer replyContainer(ConnectionFactory connectionFactory) {
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.setQueueNames(RPC_QUEUE2);
        container.setMessageListener(rpcRabbitTemplate(connectionFactory));
        return container;
    }

}
