package com.matin.online_shop.controllers;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import com.matin.online_shop.message.rabbitmq.RabbitTransactionMessage;
import com.matin.online_shop.model.Product;
import com.matin.online_shop.model.Transaction;
import com.matin.online_shop.security.services.UserDetailsImpl;
import com.matin.online_shop.services.product.ProductService;
import com.matin.online_shop.services.rabbitmq.RabbitMQSender;
import com.matin.online_shop.services.transaction.TransactionService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/product")
public class ProductController {

    @Autowired
    ProductService productService;

    @Autowired
    TransactionService transactionService;

    @Autowired
    RabbitMQSender rabbitMQSender;

    @GetMapping
    @PreAuthorize("hasRole('ADMIN') or hasRole('MODERATOR') or hasRole('USER')")
    public ResponseEntity<List<Product>> getAll() {
        List<Product> products = productService.getProducts();
        return ResponseEntity.ok().body(products);
    }

    @PostMapping(value = "/buy/{productId}")
    @PreAuthorize("hasRole('ADMIN') or hasRole('MODERATOR') or hasRole('USER')")
    public ResponseEntity<Transaction> buyProduct(@PathVariable String productId) {
        Product product = productService.getProductById(productId).get();
        double productPrice = product.getPrice();
        UserDetailsImpl userDetails = (UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        String userId = userDetails.getId();
        RabbitTransactionMessage message = new RabbitTransactionMessage(userId, productPrice);
        boolean result = rabbitMQSender.send(message);
        Transaction transaction = new Transaction(userId, productPrice, result, new Date());
        Transaction transactionWithId = transactionService.createTransaction(transaction);
        return ResponseEntity.ok().body(transactionWithId);
    }

    @PostMapping
    @PreAuthorize("hasRole('ADMIN') or hasRole('MODERATOR') or hasRole('USER')")
    public ResponseEntity<Product> addProduct(@Valid @RequestBody Product product) {
        Product saved_product = productService.createProduct(product);
        return ResponseEntity.ok().body(saved_product);
    }

    @DeleteMapping(value = "/{productId}")
    @PreAuthorize("hasRole('ADMIN') or hasRole('MODERATOR') or hasRole('USER')")
    public ResponseEntity<Void> deleteProductById(@PathVariable String productId) {
        productService.deleteProduct(productId);
        return ResponseEntity.noContent().build();
    }
}
