package com.matin.online_shop.controllers;

import java.util.stream.Collectors;

import javax.validation.Valid;

import com.matin.online_shop.dao.UserRepository;
import com.matin.online_shop.message.payload.request.UserUpdateRequest;
import com.matin.online_shop.message.payload.response.UserInfoResponse;
import com.matin.online_shop.model.User;
import com.matin.online_shop.security.services.UserDetailsImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    UserRepository userRepository;

    @PatchMapping
    public ResponseEntity<UserInfoResponse> updateUser(@Valid @RequestBody UserUpdateRequest userUpdateRequest) {
        UserDetailsImpl userDetails = (UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        String userId = userDetails.getId();
        User user = userRepository.findById(userId).get();
        System.out.println("User update information is = \n" + userUpdateRequest);
        String newEmail = userUpdateRequest.getEmail();
        String newPassword = userUpdateRequest.getPassword();
        String newFirstName = userUpdateRequest.getFirstName();
        String newLastName = userUpdateRequest.getLastName();
        String newPhoneNumber = userUpdateRequest.getPhoneNumber();
        if (newEmail != null) {
            user.setEmail(newEmail);
        }
        if (newPassword != null) {
            user.setPassword(newPassword);
        }
        if (newFirstName != null) {
            user.setFirstName(newFirstName);
        }
        if (newLastName != null) {
            user.setLastName(newLastName);
        }
        if (newPhoneNumber != null) {
            user.setPhoneNumber(newPhoneNumber);
        }
        User updatedUser = userRepository.save(user);
        UserInfoResponse response = new UserInfoResponse(updatedUser.getId(), updatedUser.getUsername(),
                updatedUser.getEmail(), updatedUser.getFirstName(), updatedUser.getLastName(),
                updatedUser.getPhoneNumber(), updatedUser.getCreationDate(), userDetails.getAuthorities().stream()
                        .map(item -> item.getAuthority())
                        .collect(Collectors.toList()));
        return ResponseEntity.ok().body(response);
    }
}
