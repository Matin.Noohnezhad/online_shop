package com.matin.online_shop.dao;

import com.matin.online_shop.model.Product;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface ProductRepository extends MongoRepository<Product, String> {

}
