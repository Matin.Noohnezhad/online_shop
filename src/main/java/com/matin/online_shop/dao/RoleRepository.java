package com.matin.online_shop.dao;

import java.util.Optional;

import com.matin.online_shop.model.ERole;
import com.matin.online_shop.model.Role;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface RoleRepository extends MongoRepository<Role, String> {
    Optional<Role> findByName(ERole name);
}
