package com.matin.online_shop.dao;

import java.util.Optional;

import com.matin.online_shop.model.User;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserRepository extends MongoRepository<User, String> {

    Optional<User> findByUsername(String username);

    Boolean existsByUsername(String username);

}
