package com.matin.online_shop.message.payload.request;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.matin.online_shop.model.validation.ValidPassword;
import com.matin.online_shop.model.validation.ValidPhoneNumber;
import com.matin.online_shop.model.validation.ValidUsername;

public class SignUpRequest {
    @NotBlank
    @ValidUsername(message = "Please enter a valid username. Username shouldn't contain speical characters.")
    private String username;

    @NotBlank
    @Email
    private String email;

    @NotBlank
    @Size(min = 10)
    @ValidPassword(message = "Please enter a valid password. Password should contain speical characters.")
    private String password;

    @NotBlank
    private String firstName;

    @NotBlank
    private String lastName;

    @NotBlank
    @ValidPhoneNumber(message = "Please enter a valid phone number")
    private String phoneNumber;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return this.phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

}
