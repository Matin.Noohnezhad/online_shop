package com.matin.online_shop.message.payload.request;

import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

import com.matin.online_shop.model.validation.ValidPassword;
import com.matin.online_shop.model.validation.ValidPhoneNumber;

public class UserUpdateRequest {
    @Email
    private String email;

    @Size(min = 10)
    @ValidPassword(message = "Please enter a valid password. Password should contain speical characters.")
    private String password;

    private String firstName;

    private String lastName;

    @ValidPhoneNumber(message = "Please enter a valid phone number")
    private String phoneNumber;

    public UserUpdateRequest(String email, String password, String firstName, String lastName, String phoneNumber) {
        this.email = email;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return this.phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Override
    public String toString() {
        return "{" +
                " email='" + getEmail() + "'" +
                ", password='" + getPassword() + "'" +
                ", firstName='" + getFirstName() + "'" +
                ", lastName='" + getLastName() + "'" +
                ", phoneNumber='" + getPhoneNumber() + "'" +
                "}";
    }

}
