package com.matin.online_shop.message.rabbitmq;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public record RabbitTransactionMessage(@JsonProperty("userId") String userId, @JsonProperty("price") Double price)
        implements Serializable {
}