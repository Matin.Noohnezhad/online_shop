package com.matin.online_shop.model;

import java.util.Date;

import javax.validation.constraints.NotBlank;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("products")
public class Product {

    @Id
    private String id;
    @NotBlank
    private String name;
    @NotBlank
    private ProductCategory category;
    @NotBlank
    private double price;
    @CreatedDate
    private Date creationDate;

    public Product() {
    }

    public Product(String id, String name, ProductCategory category, double price, Date creationDate) {
        this.id = id;
        this.name = name;
        this.category = category;
        this.price = price;
        this.creationDate = creationDate;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ProductCategory getCategory() {
        return this.category;
    }

    public void setCategory(ProductCategory category) {
        this.category = category;
    }

    public double getPrice() {
        return this.price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Date getCreationDate() {
        return this.creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public enum ProductCategory {
        DIGITAL_DEVICES,
        CLOTHES,
        VIDEO_GAMES
    }

}
