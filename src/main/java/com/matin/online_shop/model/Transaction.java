package com.matin.online_shop.model;

import java.util.Date;

import javax.validation.constraints.NotBlank;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("transactions")
public class Transaction {
    @Id
    private String id;
    @NotBlank
    private String userId;
    @NotBlank
    private double price;
    @NotBlank
    private boolean result;
    @NotBlank
    private Date creationDate;

    public Transaction() {
    }

    public Transaction(String userId, double price, boolean result, Date creationDate) {
        this.userId = userId;
        this.price = price;
        this.result = result;
        this.creationDate = creationDate;
    }

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public double getPrice() {
        return this.price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public boolean isResult() {
        return this.result;
    }

    public boolean getResult() {
        return this.result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public Date getCreationDate() {
        return this.creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

}
