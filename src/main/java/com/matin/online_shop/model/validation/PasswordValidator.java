package com.matin.online_shop.model.validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PasswordValidator implements ConstraintValidator<ValidPassword, String> {

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (value == null) {
            // user hasn't entered any value for password. (used when user wants to update
            // his(her) information.)
            return true;
        }
        // check that special characters exists in string
        Pattern p = Pattern.compile("[^A-Za-z0-9]");
        Matcher m = p.matcher(value);
        boolean b = m.find();
        return b;
    }
}
