package com.matin.online_shop.model.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PhoneNumberValidator implements ConstraintValidator<ValidPhoneNumber, String> {

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        // The Lookup API requires your phone number in E.164 format
        // E.164 formatted phone numbers must not have spaces in them
        if (value == null) {
            // user hasn't entered any value for phone number. (used when user wants to
            // update his(her) information.)
            return true;
        }
        value = value.replaceAll("[\\s()+-]", "");

        if ("".equals(value)) {
            return false;
        }
        return isValidUS(value) || isValidUK(value) || isValidGermany(value);
    }

    private boolean isValidUS(String value) {
        return value.matches("1\\d{10}");
    }

    private boolean isValidUK(String value) {
        return value.matches("44\\d{10}");
    }

    private boolean isValidGermany(String value) {
        return value.matches("49\\d{10}");
    }

}
