package com.matin.online_shop.model.validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class UsernameValidator implements ConstraintValidator<ValidUsername, String> {

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        // check that no special characters exists in string
        Pattern p = Pattern.compile("[^A-Za-z0-9]");
        Matcher m = p.matcher(value);
        boolean b = m.find();
        return !b;
    }
}
