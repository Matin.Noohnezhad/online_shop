package com.matin.online_shop.services.product;

import java.util.List;
import java.util.Optional;

import com.matin.online_shop.model.Product;

public interface ProductService {

    public Product createProduct(Product product);

    public void deleteProduct(String id);

    public List<Product> getProducts();

    public Optional<Product> getProductById(String id);
}
