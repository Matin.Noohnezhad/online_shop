package com.matin.online_shop.services.product;

import java.util.List;
import java.util.Optional;

import com.matin.online_shop.dao.ProductRepository;
import com.matin.online_shop.model.Product;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    ProductRepository productRepository;

    @Override
    public Product createProduct(Product product) {
        return productRepository.save(product);
    }

    @Override
    public void deleteProduct(String id) {
        productRepository.deleteById(id);
    }

    @Override
    public List<Product> getProducts() {
        List<Product> products = productRepository.findAll();
        return products;
    }

    @Override
    public Optional<Product> getProductById(String id) {
        Optional<Product> product = productRepository.findById(id);
        return product;
    }

}
