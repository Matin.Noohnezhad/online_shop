package com.matin.online_shop.services.rabbitmq;

import com.matin.online_shop.message.rabbitmq.RabbitTransactionMessage;
import com.matin.online_shop.message.rabbitmq.RabbitUserCreationMessage;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class RabbitMQSender {

    @Autowired
    @Qualifier("user_creation_template")
    private AmqpTemplate rabbitTemplate;

    @Autowired
    @Qualifier("rpc_template")
    private RabbitTemplate rpcRabbitTemplate;

    @Value("${onlineshop.rabbitmq.exchange}")
    private String USER_CREATION_EXCHANGE;

    @Value("${onlineshop.rabbitmq.rpc_exchange}")
    String RPC_EXCHANGE;

    @Value("${onlineshop.rabbitmq.routingkey}")
    private String ROUTING_KEY;

    @Value("${onlineshop.rabbitmq.rpc_routingkey1}")
    String RPC_ROUTING_KEY1;

    public void send(RabbitUserCreationMessage message) {
        rabbitTemplate.convertAndSend(USER_CREATION_EXCHANGE, ROUTING_KEY, message);
        System.out.println("Send msg = " + message);

    }

    public boolean send(RabbitTransactionMessage message) {
        System.out.println("sending rpc msg = " + message);
        Boolean result = (Boolean) rpcRabbitTemplate.convertSendAndReceive(RPC_EXCHANGE, RPC_ROUTING_KEY1, message);
        System.out.println("receiving result = " + result);
        return result;
    }
}
