package com.matin.online_shop.services.transaction;

import com.matin.online_shop.model.Transaction;

public interface TransactionService {

    public Transaction createTransaction(Transaction transaction);

}
