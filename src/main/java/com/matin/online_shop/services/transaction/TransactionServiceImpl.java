package com.matin.online_shop.services.transaction;

import com.matin.online_shop.dao.TransactionRepository;
import com.matin.online_shop.model.Transaction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TransactionServiceImpl implements TransactionService {

    @Autowired
    TransactionRepository transactionRepository;

    @Override
    public Transaction createTransaction(Transaction transaction) {
        Transaction transactionWithId = transactionRepository.save(transaction);
        return transactionWithId;
    }

}
