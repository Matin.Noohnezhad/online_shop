package com.matin.online_shop.model.validation;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class PasswordValidatorTests {
    private static PasswordValidator passwordValidator;

    @BeforeAll
    private static void setUp() {
        passwordValidator = new PasswordValidator();
    }

    @Test
    public void trueWhenSpecialCharExists() {
        assertTrue(passwordValidator.isValid("m@Tin93", null));
    }

    @Test
    public void falseWhenNoSpecialCharExists() {
        assertFalse(passwordValidator.isValid("MaTiN93", null));
    }

}
