package com.matin.online_shop.model.validation;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class PhoneNumberValidatorTests {

    private static PhoneNumberValidator phoneNumberValidator;

    @BeforeAll
    private static void setUp() {
        phoneNumberValidator = new PhoneNumberValidator();
    }

    @Test
    public void falseWhenEmpty() {
        assertFalse(phoneNumberValidator.isValid("", null));
    }

    @Test
    public void trueWhenUSFormatType1() {
        assertTrue(phoneNumberValidator.isValid("+1 (555) 555-1234", null));
    }
    @Test
    public void trueWhenUSFormatType2() {
        assertTrue(phoneNumberValidator.isValid("+1 555-555-1234", null));
    }
    @Test
    public void trueWhenUSFormatType3() {
        assertTrue(phoneNumberValidator.isValid("1-555-555-1234", null));
    }
    @Test
    public void trueWhenUSFormatType4() {
        assertTrue(phoneNumberValidator.isValid("15555551234", null));
    }

    @Test
    public void trueWhenUKFormatType1() {
        assertTrue(phoneNumberValidator.isValid("+44 7911 123456", null));
    }
    @Test
    public void trueWhenUKFormatType2() {
        assertTrue(phoneNumberValidator.isValid("+44-7911-123456", null));
    }
    @Test
    public void trueWhenUKFormatType3() {
        assertTrue(phoneNumberValidator.isValid("44-7911-123456", null));
    }
    @Test
    public void trueWhenUKFormatType4() {
        assertTrue(phoneNumberValidator.isValid("447911123456", null));
    }

    @Test
    public void trueWhenGermanyFormatType1() {
        assertTrue(phoneNumberValidator.isValid("+49 69 1234 5678", null));
    }

    @Test
    public void trueWhenGermanyFormatType2() {
        assertTrue(phoneNumberValidator.isValid("+49-69-1234-5678", null));
    }

    @Test
    public void trueWhenGermanyFormatType3() {
        assertTrue(phoneNumberValidator.isValid("+496912345678", null));
    }

    @Test
    public void trueWhenGermanyFormatType4() {
        assertTrue(phoneNumberValidator.isValid("496912345678", null));
    }

    @Test
    public void falseWhenRandomString(){
        assertFalse(phoneNumberValidator.isValid("random-string", null));
    }
}
