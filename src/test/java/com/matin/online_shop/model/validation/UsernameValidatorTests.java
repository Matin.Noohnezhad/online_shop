package com.matin.online_shop.model.validation;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class UsernameValidatorTests {

    private static UsernameValidator usernameValidator;

    @BeforeAll
    private static void setUp() {
        usernameValidator = new UsernameValidator();
    }

    @Test
    public void trueWhenNoSpecialCharExists() {
        assertTrue(usernameValidator.isValid("MaTiN93", null));
    }

    @Test
    public void falseWhenSpecialCharExists() {
        assertFalse(usernameValidator.isValid("m@Tin93", null));
    }
}
